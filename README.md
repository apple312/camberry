# CAMBERRY
**Raspberry Pi based remote camera control and video streaming.**

[WIKI](https://gitlab.com/apple312/camberry/-/wikis/Home-page)

Project includes:
*  device with camera and possibility to control its position
*  chassis with DC motors to remotely move device
*  software to stream video from camera via local wifi network 
*  web admin panel for this device with possibility to remotely rotate camera in the half sphere range.

<img src="https://gitlab.com/apple312/camberry/raw/master/final_web_interface.PNG" alt="drawing" width="450"/>

Used devices and technologies:
*  Raspberry Pi 3
*  1 x 180° servo mechanism WS-SG90
*  stepper motor 28BYJ-48
*  chassis with two wheels 2WD RT-5 
*  2 x DC 6V motors TT D65
*  motors driver L298N
*  RPi Camera
*  essential electronic equipment 
*  Python 3
*  Python libraries: flask, gpiozero, RPi.GPIO
*  CSS, JS, HTML

<img src="https://gitlab.com/apple312/camberry/raw/master/final_device.jpg" alt="drawing" width="450"/>


