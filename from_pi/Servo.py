from gpiozero import Servo
from time import sleep
import sys

if __name__ == "__main__":

    # ------- INIT ------------
    servo_pin = 23
    my_correction = 0.45
    max_pw = (2.0 + my_correction) / 1000
    min_pw = (1.0 - my_correction) / 1000
    servo = Servo(servo_pin, min_pulse_width=min_pw, max_pulse_width=max_pw)
    # -------------------------

    direction = int(sys.argv[1])
    file = open("servo_position", 'r')
    servo_position = float(file.readline())
    file.close()

    while True:
        if direction == -1:
            if servo_position > -0.95:
                servo_position = servo_position + direction * 0.07
        elif direction == 1:
            if servo_position < 0.95:
                servo_position = servo_position + direction * 0.07

        servo.value = servo_position

        # print("zmieniam pozycje na:")
        # print(servo_position)

        file = open("servo_position", 'w')
        file.write(str(servo_position))
        file.close()

        sleep(0.21)
