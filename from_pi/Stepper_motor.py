import RPi.GPIO as GPIO
import time
import sys

# Stepper initialization
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
StepPins = [4, 18, 27, 22]

for pin in StepPins:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)

Seq = [[1, 0, 0, 0],
       [1, 1, 0, 0],
       [0, 1, 0, 0],
       [0, 1, 1, 0],
       [0, 0, 1, 0],
       [0, 0, 1, 1],
       [0, 0, 0, 1],
       [1, 0, 0, 1]]

# stepper functions:
if __name__ == "__main__":

    Dir = int(sys.argv[1])
    i = 0
    if Dir == 1:
        while True:
            for StepCounter in range(0, 7, 1):
                for pins in range(4):
                    GPIO.output(StepPins[pins], Seq[StepCounter][pins])
                time.sleep(0.0010)

    elif Dir == -1:
        while True:
            for StepCounter in range(7, 0, -1):
                for pins in range(4):
                    GPIO.output(StepPins[pins], Seq[StepCounter][pins])
                time.sleep(0.0012)
