from flask import Flask, render_template, Response, request
from gpiozero import Servo
import time
# Raspberry Pi camera module (requires picamera package)
from camera_pi import Camera
import RPi.GPIO as GPIO
import os

# -------- DC INIT ---------------
in1 = 12
in2 = 16
in3 = 20
in4 = 21
en1 = 8
en2 = 7

GPIO.setmode(GPIO.BCM)
for pin in [in1, in2, in3, in4, en1, en2]:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    if pin == en1:
        p = GPIO.PWM(pin, 1000)
        p.start(40)
    elif pin == en2:
        q = GPIO.PWM(pin, 1000)
        q.start(40)

# --------- SERVO INIT ----------
file = open("servo_position", 'w')
file.write('0')
file.close()

app = Flask(__name__)


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('main.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/stepper_motor')
def stepper_motor():
    method = request.args.get('method')
    if method == 'left':
        os.system("python3 Stepper_motor.py " + str(-1))
    elif method == 'right':
        os.system("python3 Stepper_motor.py " + str(1))
    elif method == 'stop':
        os.system("sudo pkill -f Stepper_motor.py")
        GPIO.cleanup()
    return render_template('main.html')


@app.route('/servo')
def servo():
    method = request.args.get('method')
    if method == 'up':
        os.system("python3 Servo.py " + str(1))
    elif method == 'down':
        os.system("python3 Servo.py " + str(-1))
    elif method == 'stop':
        os.system("sudo pkill -f Servo.py")
    return render_template('main.html')


@app.route('/DC')
def DC_motors():
    GPIO.setmode(GPIO.BCM)
    for pin in [in1, in2, in3, in4, en1, en2]:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)

    method = request.args.get('method')
    if method == 'forward':
        GPIO.output(in1, 0)
        GPIO.output(in2, 1)
        GPIO.output(in3, 0)
        GPIO.output(in4, 1)
    elif method == 'backward':
        GPIO.output(in1, 1)
        GPIO.output(in2, 0)
        GPIO.output(in3, 1)
        GPIO.output(in4, 0)
    if method == 'left':
        GPIO.output(in1, 1)
        GPIO.output(in2, 0)
        GPIO.output(in3, 0)
        GPIO.output(in4, 1)
    elif method == 'right':
        GPIO.output(in1, 0)
        GPIO.output(in2, 1)
        GPIO.output(in3, 1)
        GPIO.output(in4, 0)
    elif method == 'stop':
        GPIO.output(in1, 0)
        GPIO.output(in2, 0)
        GPIO.output(in3, 0)
        GPIO.output(in4, 0)
    return render_template('main.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
